== Lista de materiales

// Materiales necesarios por categoría, ordenados alfabéticamente

=== Cocina y comida

* Tenedores

=== Mobiliario

* Hamaca
* Mesas
* Mesa supletoria para juegos
* Parrilla
* Sillas
* Sombrillas

=== Diversión

* Radiocassette
* Slack line
