== Posibles localizaciones

// Describir lugar y poner alguna foto. ¡Ojo!

=== Rivera del Huéznar

Bonito lugar en la Sierra Norte de Sevilla.

.Ribera del río Huéznar a su paso por La Fundición. https://commons.wikimedia.org/wiki/File:Ribera_del_Hueznar_01.jpg[Créditos].
image::images/576px-Ribera_del_Hueznar_01.jpg[Ribera del Huéznar]

=== Orilla de la presa de la albuera

Una buena ubicación para pasar una estupenda tarde de barbacoa. Al frescor de la brisa del lago.

.La albuera de Zafra. https://www.flickr.com/photos/49651443@N06/6708981155[Créditos]
image::images/zafra.jpg[Albuera de zafra]

=== Pinares de la Puebla

Perfecto para pasar un buen día de barbacoa donde incluyen merenderos bajo la sombra de los pinares.

.Pinares de la Puebla del Río y Aznalcázar. http://www.caminosvivos.com/recurso-detalle/3596/pinares-de-la-puebla-del-rio-y-aznalcazar[Créditos].
image::images/Pinares_Puebla.jpg[Pinares de la Puebla]

